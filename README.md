# What is this app for? #
**The Catalog** is a native Android application for a book keeper to keep track of the books in a library. 

# Features #
* List all the books in the library
* Add Books
* Delete all books
* Display details about a book
* Delete a specific book
* Update book information
* Share a book using the ShareActionProvider
* Check out a book

# Support for Android Versions #
Android -v 4.0 (Ice Cream Sandwich) - 4.4 (KitKat)

# Installation/Build #
You can install/build this application using Gradle. Once you clone the project run ./gradlew installDebug

# Contact info #
You can contact me at *suzette.gm@gmail.com* 

# App Screenshots #

![Screenshot_2015-02-18-00-00-21.png](https://bitbucket.org/repo/MeRnKg/images/4229190331-Screenshot_2015-02-18-00-00-21.png)

![Screenshot_2015-02-18-00-00-07.png](https://bitbucket.org/repo/MeRnKg/images/3996787519-Screenshot_2015-02-18-00-00-07.png)

![Screenshot_2015-02-18-00-00-17.png](https://bitbucket.org/repo/MeRnKg/images/1738859102-Screenshot_2015-02-18-00-00-17.png)

![Screenshot_2015-02-18-00-00-26.png](https://bitbucket.org/repo/MeRnKg/images/917636957-Screenshot_2015-02-18-00-00-26.png)

![Screenshot_2015-02-18-00-00-39.png](https://bitbucket.org/repo/MeRnKg/images/173598532-Screenshot_2015-02-18-00-00-39.png)

![Screenshot_2015-02-18-00-00-47.png](https://bitbucket.org/repo/MeRnKg/images/3469479837-Screenshot_2015-02-18-00-00-47.png)