package com.prolific.thecatalog;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.TextView;

import com.prolific.thecatalog.model.Book;
import com.prolific.thecatalog.rest.AppClient;

/*  This fragment holds a ListView which displays the books in the catalog. BooksFragment has the methods to get
 *  the list of books from the server, add books to the list and remove all of them. */

public class BookFragment extends ListFragment { 

	OnBookSelectedListener listenerCallback;
	private Typeface fontStyle;
	private Dialog addBookDialog;
	public static List<Book> booksList;
	private static CustomAdapter adapter;

	/* Interface that handles onClickListener */
	public interface OnBookSelectedListener {
		/** Called by BookFragment when a list item is selected */
		public void onBookClicked(int position);
	}


	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		this.setHasOptionsMenu(true);

		/* Get books from server */
		getBooksFromServer();//
	}


	@Override
	public void onStart() {

		super.onStart();
		/* When the UI is visible set the ListView to hightlight the option selected */
		if (getFragmentManager().findFragmentById(R.id.details_fragment) != null) { //if in tablet mode
			getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		}

		/* Getting an action bar instance */
		final ActionBar bar = this.getActivity().getActionBar();
		bar.setDisplayShowTitleEnabled(false);
		bar.setDisplayShowCustomEnabled(true);
		bar.setDisplayHomeAsUpEnabled(false);
		bar.setCustomView(R.layout.actionbar_view);

		/* Styling the action bar text font */
		View actionView = bar.getCustomView();
		TextView logo = (TextView) actionView.findViewById(R.id.actionBarLogo);
		fontStyle = Typeface.createFromAsset(actionView.getContext().getAssets(), MainActivity.FONT_STYLE);
		logo.setTypeface(fontStyle);

		getListView().setEmptyView(setEmptyListView());


	}

	/* Makes sure that the activity implements the Listener */
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		try {
			listenerCallback = (OnBookSelectedListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException();
		}
	}


	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {

		listenerCallback.onBookClicked(position);
		getListView().setItemChecked(position, true);

	}



	/* Gets a list of books from the server and updates the ListView using the custom adapter */
	private void getBooksFromServer(){

		AppClient.getRestClient().getBooks(new Callback<List<Book>>(){

			@Override
			public void failure(RetrofitError error) {
				Log.v("debug", error.getMessage());
				errorDialog();
			}

			@Override
			public void success(List<Book> list, Response arg1) {

				if(list != null){
					booksList = list;
				}
				else{
					booksList = new ArrayList<Book>(); //in case we get a null list
				}

				updateAdapter();

			}
		});
	}

	/* Updates the content of the ListView */
	private void updateAdapter() {

		/* Custom List View */
		Collections.sort(booksList);
		adapter = new CustomAdapter(this.getActivity().getApplicationContext(), booksList);
		setListAdapter(adapter);

	}


	/* Updates adapter if list has changed */
	public static void updateAdapter(Book response){

		booksList.add(response);
		Collections.sort(booksList);
		adapter.notifyDataSetChanged();
	}


	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.main, menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()){

		/*Handles 'add book' action */
		case R.id.addBook: { 
			addBookToCatalog();
			return true;
		}

		/*Handles 'remove all books' action */
		case R.id.delete_all: {  
			confirmationDialog(getResources().getString(R.string.delete_all_entries),getResources().getString(R.string.confirm),getResources().getString(R.string.cancel), "delete");
			return true;
		}

		}
		return super.onOptionsItemSelected(item);
	}


	/* Adds book to the list */
	private void addBookToCatalog() {

		/* Custom Dialog */
		addBookDialog = new Dialog(this.getActivity());
		addBookDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		addBookDialog.setContentView(R.layout.add_book_view);


		// Form fields
		TextView dialogHeader = (TextView) addBookDialog.findViewById(R.id.confirmMessage);
		dialogHeader.setTypeface(fontStyle);

		final TextView bookName = (TextView) addBookDialog.findViewById(R.id.bookName);
		bookName.setTypeface(fontStyle);

		final EditText bookAuthor = (EditText) addBookDialog.findViewById(R.id.bookAuthor);
		bookAuthor.setTypeface(fontStyle);

		final EditText bookPublisher = (EditText) addBookDialog.findViewById(R.id.bookPublisher);
		bookPublisher.setTypeface(fontStyle);

		final EditText bookTags = (EditText) addBookDialog.findViewById(R.id.bookTags);
		bookTags.setTypeface(fontStyle);

		// Submit button clicked

		Button yesButton = (Button) addBookDialog.findViewById(R.id.yes);
		yesButton.setTypeface(fontStyle);
		yesButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				if(bookName.getText().toString().length() == 0 ){
					bookName.setError( "Book Title is required!" );
				}

				if(bookAuthor.getText().toString().length() == 0 ){
					bookAuthor.setError( "Author name is required!" );
				}

				if(bookPublisher.getText().toString().length() == 0 ){
					bookPublisher.setError( "Publisher is required!" );
				}

				if(bookTags.getText().toString().length() == 0 ){
					bookTags.setError( "Book Categories is required!" );
				}

				else{

					/* Form is complete and ready to send to the server */
					/* Send book to server */

					Book bookInfo =new Book(bookName.getText().toString(), bookAuthor.getText().toString(), bookPublisher.getText().toString(), bookTags.getText().toString(), null, null, null);
					AppClient.getRestClient().submitBook(bookInfo, new Callback<Book>(){

						@Override
						public void failure(RetrofitError error) {
							Log.v("debug", error.getMessage());
							errorDialog();
						}

						@Override
						public void success(Book response, Response arg1) {
							updateAdapter(response);
							addBookDialog.dismiss();

						}
					});


				}
			}
		});

		// Cancel button clicked

		Button noButton = (Button) addBookDialog.findViewById(R.id.no);
		noButton.setTypeface(fontStyle);
		noButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				if(bookName.getText().toString().length() != 0 || bookAuthor.getText().toString().length() != 0 || bookPublisher.getText().toString().length() != 0 || bookTags.getText().toString().length() != 0){
					confirmationDialog(getResources().getString(R.string.incomplete_form), getResources().getString(R.string.continue_form), getResources().getString(R.string.discard_form), "form");
				}
				else{
					/* Form is blank, dismiss dialog */
					addBookDialog.dismiss();
				}
			}
		});


		addBookDialog.show();

	}

	private void confirmationDialog(String dialogMessage, String confirmButton, String cancelButton, final String optionSelected) {

		/* Custom Dialog */
		final Dialog dialog = new Dialog(this.getActivity());
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.alertdialog_view);

		TextView dialogText = (TextView) dialog.findViewById(R.id.confirmMessage);
		dialogText.setText(dialogMessage);
		dialogText.setTypeface(fontStyle);

		// Confirmation button clicked 
		Button yesButton = (Button) dialog.findViewById(R.id.yes);
		yesButton.setText(confirmButton);
		yesButton.setTypeface(fontStyle);
		yesButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				if(optionSelected.compareTo("form") == 0){
					/* Goes back to the form */
					dialog.dismiss();
				}
				else if(optionSelected.compareTo("delete") == 0){

					/* User has confirmed that wants to delete everything in the Catalog - Code Here*/

					AppClient.getRestClient().deleteCatalog(new Callback<String>(){

						@Override
						public void failure(RetrofitError error) {
							Log.v("debug", error.getMessage());
							errorDialog();
						}

						@Override
						public void success(String response, Response arg1) {
							getBooksFromServer();
							dialog.dismiss(); 

						}
					});
				}

			}
		});

		// Cancel button clicked
		Button noButton = (Button) dialog.findViewById(R.id.no);
		noButton.setText(cancelButton);
		noButton.setTypeface(fontStyle);
		noButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				if(optionSelected.compareTo("form") == 0){
					dialog.dismiss();
					if(addBookDialog!=null){
						addBookDialog.dismiss();
					}
				}
				else if(optionSelected.compareTo("delete") == 0){

					dialog.dismiss();
				}

			}
		});


		dialog.show();

	}

	/* Adds a child view to the ListView. TextView will be displayed when the list is empty */
	private TextView setEmptyListView() {

		TextView emptyView = new TextView(getActivity());
		emptyView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		emptyView.setTextColor(getResources().getColor(R.color.gray));
		emptyView.setText(getResources().getString(R.string.empty_list));
		emptyView.setTextSize(20);
		emptyView.setVisibility(View.GONE);
		emptyView.setGravity(Gravity.CENTER);
		emptyView.setTypeface(fontStyle);

		/* Adds child view to the parent view (ListView) */
		((ViewGroup) getListView().getParent()).addView(emptyView);

		return emptyView;
	}

	/* Error Dialog */
	private void errorDialog() {

		/* Custom Dialog */
		final Dialog dialog = new Dialog(this.getActivity());
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.errordialog_view);

		TextView dialogText = (TextView) dialog.findViewById(R.id.errorMessage);
		dialogText.setTypeface(fontStyle);

		Button backButton = (Button) dialog.findViewById(R.id.back);
		backButton.setTypeface(fontStyle);
		backButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				/* Back */
				dialog.dismiss();

			}
		});

		dialog.show();

	}


}