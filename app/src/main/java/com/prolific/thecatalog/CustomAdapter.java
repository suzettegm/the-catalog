package com.prolific.thecatalog;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.prolific.thecatalog.model.Book;

/* CustomAdapter for the ListView used in BooksFragment. Used to set custom layouts and manage the data displayed on each row of the list */

public class CustomAdapter extends ArrayAdapter<Book> {

	private final Context context;
	private final List<Book> itemsArrayList;

	public CustomAdapter(Context context, List<Book> itemsArrayList) {

		super(context, R.layout.list_fragment, (ArrayList<Book>)itemsArrayList);
		this.context = context;
		this.itemsArrayList = itemsArrayList;
	}

	/* Set row view */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.list_item, parent, false);

		/* TextView for Book Title */
		TextView bookTitle=(TextView) rowView.findViewById(R.id.title);
		Typeface face = Typeface.createFromAsset(rowView.getContext().getAssets(), "font/Raleway-Medium.ttf");
		bookTitle.setTypeface(face);
		String title = itemsArrayList.get(position).getBookTitle();
		if(title != null){
			bookTitle.setText(title);
		}

		/* TextView for Book Author */
		TextView authorName=(TextView) rowView.findViewById(R.id.author);
		String author = itemsArrayList.get(position).getAuthor();
		if(author != null){
			authorName.setText(author);
		}

		face = Typeface.createFromAsset(rowView.getContext().getAssets(), MainActivity.FONT_STYLE);
		authorName.setTypeface(face);

		return rowView;

	}

	public List<Book> getArray(){
		return itemsArrayList;
	}
}