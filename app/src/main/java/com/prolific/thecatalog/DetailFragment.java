
package com.prolific.thecatalog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import android.app.ActionBar;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ShareActionProvider;
import android.widget.TextView;

import com.prolific.thecatalog.model.Book;
import com.prolific.thecatalog.model.BookRecord;
import com.prolific.thecatalog.rest.AppClient;

/* This fragments handles update, checkout, delete, and displays the details about a specific book. */ 

public class DetailFragment extends Fragment {

	final static String BOOK_POINTER = "position";
	private ShareActionProvider mShareActionProvider;
	private Typeface fontStyle;
	int bookPointer = -1;

	private EditText bookName = null;
	private EditText bookAuthor = null;
	private EditText bookPublisher = null;
	private EditText bookTags = null;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		View fragmentView = inflater.inflate(R.layout.details_view, container, false);

		if (savedInstanceState != null) {
			bookPointer = savedInstanceState.getInt(BOOK_POINTER);
		}

		this.setHasOptionsMenu(true);
		fontStyle = Typeface.createFromAsset(this.getActivity().getApplicationContext().getAssets(), MainActivity.FONT_STYLE);

		/* OnClickListener for the delete book button */
		Button deleteBook = (Button) fragmentView.findViewById(R.id.delete);
		deleteBook.setTypeface(fontStyle);
		deleteBook.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				detailDialog(getResources().getString(R.string.delete_entry), getResources().getString(R.string.delete_button), getResources().getString(R.string.cancel), "delete", R.layout.alertdialog_view);

			}
		});

		/* OnClickListener for the update information button */
		Button updateInfo = (Button) fragmentView.findViewById(R.id.update);
		updateInfo.setTypeface(fontStyle);
		updateInfo.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				detailDialog(getResources().getString(R.string.update_book), getResources().getString(R.string.update_button), getResources().getString(R.string.cancel), "update", R.layout.add_book_view);


			}
		});

		/* OnClickListener for the checkout button */
		Button checkoutBook = (Button) fragmentView.findViewById(R.id.checkoutButton);
		checkoutBook.setTypeface(fontStyle);
		checkoutBook.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				detailDialog(getResources().getString(R.string.checkout_message), getResources().getString(R.string.confirm), getResources().getString(R.string.cancel), "checkout", R.layout.checkout_view);


			}
		});

		return fragmentView;
	}



	@Override
	public void onStart() {
		super.onStart();

		/* Updates the UI info based on the row selected */
		Bundle data = getArguments();
		if (data != null) {
			updateDetails(data.getInt(BOOK_POINTER));
		} else if (bookPointer != -1) { 
			updateDetails(bookPointer);
		}


		/* Getting an action bar instance */
		final ActionBar bar = this.getActivity().getActionBar();
		bar.setDisplayHomeAsUpEnabled(true);
		bar.setCustomView(R.layout.actionbar_view);

		/* Styling the action bar text font */
		View actionView = bar.getCustomView();
		TextView logo = (TextView) actionView.findViewById(R.id.actionBarLogo);
		logo.setText("Detail");
		logo.setTypeface(fontStyle);
	}

	@Override
	public void onSaveInstanceState(Bundle saveState) {
		super.onSaveInstanceState(saveState);

		/* Always save the state */
		saveState.putInt(BOOK_POINTER, bookPointer);
	}


	/* Universal dialog. It has a TextView and two buttons */

	private void detailDialog(String outputMessage, String confirmButton, String cancelButton, final String optionSelected, int layoutView){

		/* Custom Dialog */
		final Dialog dialog = new Dialog(this.getActivity());
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(layoutView);

		TextView dialogText = (TextView) dialog.findViewById(R.id.confirmMessage);
		dialogText.setText(outputMessage);
		dialogText.setTypeface(fontStyle);

		if(optionSelected.compareTo("update") == 0){

			TextView bookTitle = (TextView) getActivity().findViewById(R.id.details);
			TextView author = (TextView) getActivity().findViewById(R.id.author);
			TextView publisher = (TextView) getActivity().findViewById(R.id.publisher);
			TextView tags = (TextView) getActivity().findViewById(R.id.tags);

			List<Book> list = BookFragment.booksList;
			bookName = (EditText) dialog.findViewById(R.id.bookName);
			String titleList = list.get(bookPointer).getBookTitle();
			if(titleList != null){
				bookName.setText(titleList);
			}
			bookAuthor = (EditText) dialog.findViewById(R.id.bookAuthor);
			String authorList = list.get(bookPointer).getAuthor();
			if(authorList != null){
				bookAuthor.setText(authorList);
			}
			bookPublisher = (EditText) dialog.findViewById(R.id.bookPublisher);
			String publisherList = list.get(bookPointer).getPublisher();
			if(publisherList != null){
				bookPublisher.setText(publisherList);
			}
			bookTags = (EditText) dialog.findViewById(R.id.bookTags);
			String tagsList = list.get(bookPointer).getCategories();
			if(tagsList != null){
				bookTags.setText(tagsList);
			}

		}

		// Confirmation button clicked
		Button yesButton = (Button) dialog.findViewById(R.id.yes);
		yesButton.setText(confirmButton);
		yesButton.setTypeface(fontStyle);
		yesButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				if(optionSelected.compareTo("delete") == 0){

					/* Delete Book from server, confirmed by user */
					deleteBook(dialog);
				}

				if(optionSelected.compareTo("update") == 0){

					if(bookName.getText().toString().length() == 0 ){
						bookName.setError( "Book Title is required!" );
					}

					if(bookAuthor.getText().toString().length() == 0 ){
						bookAuthor.setError( "Author name is required!" );
					}

					if(bookPublisher.getText().toString().length() == 0 ){
						bookPublisher.setError( "Publisher is required!" );
					}

					if(bookTags.getText().toString().length() == 0 ){
						bookTags.setError( "Book Categories is required!" );
					}

					else{
						/* Form is complete and ready to send to the server */
						Log.v("debug", "update book information");
						updateBook(dialog);
					}


				}

				if(optionSelected.compareTo("checkout") == 0){

					/* Checkout book */
					final EditText userName = (EditText) dialog.findViewById(R.id.userName);
					userName.setTypeface(fontStyle);

					if(userName.getText().toString().length() == 0 ){
						userName.setError( "Name is required!" );
					}
					else{

						/* Ready for checkout */
						String checkOutDate = new String();
						Date date = new Date();
						SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss zzz");
						checkOutDate = sdf.format(date);
						checkoutBook(dialog, new BookRecord(userName.getText().toString(), checkOutDate));

					}

				}

			}

		});

		// Cancel button clicked
		Button noButton = (Button) dialog.findViewById(R.id.no);
		noButton.setText(cancelButton);
		noButton.setTypeface(fontStyle);
		noButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				if(optionSelected.compareTo("update") == 0){

					if(bookName.getText().toString().length() != 0 || bookAuthor.getText().toString().length() != 0 || bookPublisher.getText().toString().length() != 0 || bookTags.getText().toString().length() != 0){
						confirmationDialog(getResources().getString(R.string.incomplete_form), getResources().getString(R.string.continue_form), getResources().getString(R.string.discard_form), dialog);
					}
					else{
						/* Form is blank, dismiss dialog */
						dialog.dismiss();
						Log.v("debug", "form is blank so dismiss");
					}
				}
				else{
					dialog.dismiss();
				}
			}
		});


		dialog.show();

	}

	private void confirmationDialog(String dialogMessage, String confirmButton, String cancelButton, final Dialog updateDialog) {

		/* Custom Dialog */
		final Dialog dialog = new Dialog(this.getActivity());
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.alertdialog_view);

		TextView dialogText = (TextView) dialog.findViewById(R.id.confirmMessage);
		dialogText.setText(dialogMessage);
		dialogText.setTypeface(fontStyle);

		Button yesButton = (Button) dialog.findViewById(R.id.yes);
		yesButton.setText(confirmButton);
		yesButton.setTypeface(fontStyle);
		yesButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				/* Goes back to the form */
				dialog.dismiss();
				Log.v("debug", "back to the form");

			}
		});

		Button noButton = (Button) dialog.findViewById(R.id.no);
		noButton.setText(cancelButton);
		noButton.setTypeface(fontStyle);
		noButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				Log.v("debug", "dismiss both dialogs");
				dialog.dismiss();
				if(updateDialog!=null){
					updateDialog.dismiss();
				}


			}
		});


		dialog.show();


	}

	/* Handles the delete call to the end point. Updates List */
	private void deleteBook(final Dialog dialog){

		AppClient.getRestClient().deleteBook(BookFragment.booksList.get(bookPointer).getId(), new Callback<Book>(){

			@Override
			public void failure(RetrofitError error) {
				Log.v("debug", error.getMessage());
				errorDialog();
			}

			@Override
			public void success(Book response, Response arg1) {
				BookFragment.booksList.remove(bookPointer);
				Collections.sort(BookFragment.booksList);
				dialog.dismiss();
				popFragmentFromStack();

			}
		});
	}

	/* Handles the update call to the end point. Updates List */
	private void updateBook(final Dialog dialog){

		Book bookInfo =new Book(bookName.getText().toString(), bookAuthor.getText().toString(), bookPublisher.getText().toString(), bookTags.getText().toString(), null, null, BookFragment.booksList.get(bookPointer).getId());
		AppClient.getRestClient().updateBook(BookFragment.booksList.get(bookPointer).getId(), bookInfo, new Callback<Book>(){

			@Override
			public void failure(RetrofitError error) {
				Log.v("debug", error.getMessage());
				errorDialog();
			}

			@Override
			public void success(Book response, Response arg1) {
				BookFragment.booksList.get(bookPointer).setBookTitle(response.getBookTitle());
				BookFragment.booksList.get(bookPointer).setAuthor(response.getAuthor());
				BookFragment.booksList.get(bookPointer).setCategories(response.getCategories());
				BookFragment.booksList.get(bookPointer).setPublisher(response.getPublisher());
				Collections.sort(BookFragment.booksList);
				dialog.dismiss();
				popFragmentFromStack();
			}
		});
	}

	/* Handles the checkout call to the end point. Updates List */
	private void checkoutBook(final Dialog dialog, BookRecord bookRecord){

		AppClient.getRestClient().checkoutBook((BookFragment.booksList.get(bookPointer).getId()), bookRecord, new Callback<Book>(){

			@Override
			public void failure(RetrofitError error) {
				Log.v("debug", error.getMessage());
				errorDialog();
			}

			@Override
			public void success(Book response, Response arg1) {

				BookFragment.booksList.get(bookPointer).setLastCheckedOutBy(response.getLastCheckedOutBy());
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				String reportDate = new String();
				Date date = null;

				try {

					date = sdf.parse(response.getLastCheckedOut().toString());
					sdf = new SimpleDateFormat("MMMM dd, yyyy @ HH:mm:ss");
					reportDate = sdf.format(date);
					BookFragment.booksList.get(bookPointer).setLastCheckedOut(reportDate);
					dialog.dismiss();
					popFragmentFromStack();

				} catch (ParseException e) {
					reportDate = "Unknown";
					BookFragment.booksList.get(bookPointer).setLastCheckedOut(reportDate);
					dialog.dismiss();
					popFragmentFromStack();
					e.printStackTrace();
				}

			}
		});
	}

	/* Updates detail_fragment layout with the book detail clicked*/
	public void updateDetails(int position) {

		List<Book> list = BookFragment.booksList;
		TextView author = (TextView) getActivity().findViewById(R.id.author);
		String authorList = list.get(position).getAuthor();
		if(authorList != null){
			author.setText("Author: " + authorList);
		}
		author.setTypeface(fontStyle);
		TextView publisher = (TextView) getActivity().findViewById(R.id.publisher);
		String publisherList = list.get(position).getPublisher();
		if(publisherList != null){
			publisher.setText("Publisher: " + publisherList);
		}
		publisher.setTypeface(fontStyle);
		TextView tags = (TextView) getActivity().findViewById(R.id.tags);
		String tagsList = list.get(position).getCategories();
		if(tagsList != null){
			tags.setText("Tags: " + tagsList);
		}
		tags.setTypeface(fontStyle);
		TextView checkoutInfo = (TextView) getActivity().findViewById(R.id.checkoutInfo);
		String checkedOutByList = list.get(position).getLastCheckedOutBy();
		String lastCheckedOut = list.get(position).getLastCheckedOut();

		if(checkedOutByList == null || lastCheckedOut == null){
			checkoutInfo.setText("Available for check out");
		}
		else{
			checkoutInfo.setText("Checked Out by: "+ checkedOutByList + " on " + lastCheckedOut);
		}

		checkoutInfo.setTypeface(fontStyle);

		fontStyle = Typeface.createFromAsset(this.getActivity().getApplicationContext().getAssets(), "font/Raleway-Medium.ttf");
		TextView bookTitle = (TextView) getActivity().findViewById(R.id.details);
		String titleList = list.get(position).getBookTitle();
		if(titleList != null){
			bookTitle.setText(list.get(position).getBookTitle());
		}

		bookTitle.setTypeface(fontStyle);
		bookPointer = position;
	}

	// Menu
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.detail_menu, menu);

		// Locate MenuItem with ShareActionProvider
		MenuItem item = menu.findItem(R.id.menu_item_share);
		// Fetch and store ShareActionProvider
		mShareActionProvider = (ShareActionProvider) item.getActionProvider();
		setShareIntent(this.getDefaultShareIntent());

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		switch(item.getItemId()){

		case android.R.id.home: {  

			popFragmentFromStack();
			return true;
		}

		}
		return super.onOptionsItemSelected(item);

	}

	/* Pops fragment from stack */
	private void popFragmentFromStack(){
		android.support.v4.app.FragmentManager fm = getActivity().getSupportFragmentManager();
		fm.popBackStack ("detail_fragment", FragmentManager.POP_BACK_STACK_INCLUSIVE);
	}

	/* Call to update the share intent used by ShareActionProvider */
	private void setShareIntent(Intent shareIntent) {
		if (mShareActionProvider != null) {
			mShareActionProvider.setShareIntent(shareIntent);
		}
	}

	/* Returns intent to share using ShareActionProvider */
	private Intent getDefaultShareIntent(){

		Intent intent = new Intent(Intent.ACTION_SEND);
		intent.setType("text/plain");
		intent.putExtra(Intent.EXTRA_SUBJECT, "Check out this amazing book!");

		String checkoutMessage = new String();

		if(BookFragment.booksList.get(bookPointer).getLastCheckedOutBy() == null || BookFragment.booksList.get(bookPointer).getLastCheckedOut() == null){
			checkoutMessage = "Available for checkout";
		}
		else{
			checkoutMessage = "Checked out by " + BookFragment.booksList.get(bookPointer).getLastCheckedOutBy() + " on " + BookFragment.booksList.get(bookPointer).getLastCheckedOut();
		}

		String messageToShare = "Title: " + BookFragment.booksList.get(bookPointer).getBookTitle() + "\n" +
				"Author: " + BookFragment.booksList.get(bookPointer).getAuthor() + "\n" +
				"Publisher: " + BookFragment.booksList.get(bookPointer).getPublisher() + "\n" +
				"Category: " + BookFragment.booksList.get(bookPointer).getCategories() + "\n" +
				checkoutMessage; //

		intent.putExtra(Intent.EXTRA_TEXT, messageToShare); 
		return intent;
	}


	/* Error Dialog */
	private void errorDialog() {

		/* Custom Dialog */
		final Dialog dialog = new Dialog(this.getActivity());
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.errordialog_view);

		TextView dialogText = (TextView) dialog.findViewById(R.id.errorMessage);
		dialogText.setTypeface(fontStyle);

		Button backButton = (Button) dialog.findViewById(R.id.back);
		backButton.setTypeface(fontStyle);
		backButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				/* Back */
				dialog.dismiss();

			}
		});

		dialog.show();

	}



}