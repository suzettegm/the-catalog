package com.prolific.thecatalog;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.widget.TextView;

/* Activity for splash screen */

public class SplashView extends Activity {

	// Splash screen timer
	private static int SPLASH_TIME_OUT = 2000;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);

		/* Styling splash text */
		TextView logo = (TextView) findViewById(R.id.splashLogo);
		Typeface face = Typeface.createFromAsset(getAssets(), "font/Raleway-Regular.ttf");
		logo.setTypeface(face);

		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {
				Intent i = new Intent(SplashView.this, MainActivity.class);
				startActivity(i);
				overridePendingTransition(R.anim.fade_in, R.anim.fade_out); //add fade transition between activities
			}
		}, SPLASH_TIME_OUT);

	}
}
