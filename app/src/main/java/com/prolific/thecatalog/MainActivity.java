package com.prolific.thecatalog;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;

/* Main Fragment Activity used to hold the fragments and implements the row click listener for BookFragment */

public class MainActivity extends FragmentActivity implements BookFragment.OnBookSelectedListener  {

	final static String FONT_STYLE ="font/Raleway-Regular.ttf";

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_books);

		/* If fragment was already created don't recreate object */
		if (savedInstanceState != null) {
			return;
		}

		/* Create a BookFragment object which will be added to the fragment view */
		BookFragment booksFragment = new BookFragment();
		getSupportFragmentManager().beginTransaction().add(R.id.fragment_view, booksFragment).commit();



	}

	/* Handles click action for each book row */
	public void onBookClicked(int position) {

		/* Verify whether or not a DetailsFragment exists. This will help understand if is in tablet mode or phone mode */
		DetailFragment detailsFragment = (DetailFragment) getSupportFragmentManager().findFragmentById(R.id.details_fragment);

		if (detailsFragment != null) { //exists
			detailsFragment.updateDetails(position);

		} else {
			//phone mode
			DetailFragment newDetailsFragment = new DetailFragment();
			Bundle args = new Bundle();
			args.putInt(DetailFragment.BOOK_POINTER, position);
			newDetailsFragment.setArguments(args);
			FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
			transaction.replace(R.id.fragment_view, newDetailsFragment);
			transaction.addToBackStack("detail_fragment");
			transaction.commit();
		}
	}


}
