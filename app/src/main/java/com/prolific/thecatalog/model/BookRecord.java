package com.prolific.thecatalog.model;

/* This object holds information needed to checkout a book. This will be the object send to the server for the checkout function */

public class BookRecord {

	private String lastCheckedOutBy;
	private String lastCheckedOut;

	public BookRecord(String lastCheckedOutBy, String lastCheckedOut){
		this.lastCheckedOutBy = lastCheckedOutBy;
		this.lastCheckedOut = lastCheckedOut;
	}


	public String getLastCheckedOutBy() {
		return lastCheckedOutBy;
	}

	public void setLastCheckedOutBy(String lastCheckedOutBy) {
		this.lastCheckedOutBy = lastCheckedOutBy;
	}

	public String getLastCheckedOut() {
		return lastCheckedOut;
	}

	public void setLastCheckedOut(String lastCheckedOut) {
		this.lastCheckedOut = lastCheckedOut;
	}


}
