package com.prolific.thecatalog.model;

/* This object represents a book with all it's information */

public class Book implements Comparable{


	private String title;
	private String author;
	private String publisher;
	private String categories;
	private String lastCheckedOutBy;
	private String lastCheckedOut;
	private String  id;

	public Book(String title, String author, String publisher, String categories, String lastCheckedOutBy, String lastCheckedOut, String id){

		this.title = title;
		this.author = author;
		this.publisher = publisher;
		this.categories = categories;
		this.lastCheckedOutBy = lastCheckedOutBy;
		this.lastCheckedOut = lastCheckedOut;
		this.id = id;

	}

	public String getBookTitle() {
		return title;
	}

	public void setBookTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public String getCategories() {
		return categories;
	}

	public void setCategories(String categories) {
		this.categories = categories;
	}

	public String getLastCheckedOutBy() {
		return lastCheckedOutBy;
	}

	public void setLastCheckedOutBy(String lastCheckedOutBy) {
		this.lastCheckedOutBy = lastCheckedOutBy;
	}

	public String getLastCheckedOut() {
		return lastCheckedOut;
	}

	public void setLastCheckedOut(String lastCheckedOut) {
		this.lastCheckedOut = lastCheckedOut;
	}

	public String getId(){
		return id;
	}

	public void setId(String newId){
		id = newId;
	}

	@Override
	public int compareTo(Object another) {

		return this.getBookTitle().compareTo(((Book)another).getBookTitle());

	}


}
