package com.prolific.thecatalog.rest;

import java.util.List;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;

import com.prolific.thecatalog.model.Book;
import com.prolific.thecatalog.model.BookRecord;
/* Interface to manage REST calls */

public interface CatalogAPI {

	@GET("/books")
	void getBooks(Callback<List<Book>> callback);

	@POST("/books/")
	void submitBook(@Body Book bookInfo, Callback<Book> callBack);

	@PUT("/books/{id}")
	void updateBook(@Path("id") String id, @Body Book updateInfo, Callback<Book> callBack);

	@PUT("/books/{id}")
	void checkoutBook(@Path("id") String id, @Body BookRecord bookRecord,  Callback<Book> callBack);

	@DELETE("/books/{id}")
	void deleteBook(@Path("id") String id, Callback<Book> callBack);

	@DELETE("/clean")
	void deleteCatalog(Callback<String> callBack);


}
