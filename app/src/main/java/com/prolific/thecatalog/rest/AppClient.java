package com.prolific.thecatalog.rest;


import retrofit.RestAdapter;

/* This class creates a rest adapter which will be used to make the end point calls */

public class AppClient {

	private static String END_POINT =  "http://thecatalogapp.appspot.com";
	public static CatalogAPI restClient = null;

	public static CatalogAPI getRestClient() {

		if(restClient == null){
			RestAdapter restAdapter = new RestAdapter.Builder()
			.setEndpoint(END_POINT)
			.build();

			restClient = restAdapter.create(CatalogAPI.class);

			return restClient;
		}
		else{
			return restClient;
		}
	}

}
